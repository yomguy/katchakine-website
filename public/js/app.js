var VueMasonryPlugin = window["vue-masonry-plugin"].VueMasonryPlugin
Vue.use(VueMasonryPlugin)

new Vue({
  el: '#app',
  vuetify: new Vuetify(),
  data () {
    return {
      dates: [
        {
          src: '',
          img: '',
          title: 'Bordeaux - 5 juillet 2025',
          text: 'Festival - Parc des Expositions',
          type: 'text'
        },
        {
          src: 'assets/img/Katchakine (Richard Bellia, oct 2024)-997p.jpeg',
          img: '',
          title: 'Portrait',
          text: 'par Richard Bellia, octobre 2024',
          type: 'image',
          link: '',
        },
        {
          src: 'assets/img/bournot-24.jpg',
          img: '',
          title: 'Aubenas - 19 octobre 2024',
          text: 'Le Bournot, 1ere partie de Kas Product',
          type: 'image',
          link: 'https://www.sallelebournot.fr/KAS-PRODUCT.html',
        },
        {
          src: 'assets/img/altermachine-stage.jpeg',
          img: '',
          title: 'Paris - 7 septembre 2024',
          text: 'Soirée Altermachine - La Dame de Canton',
          type: 'image'
        },
        {
          src: 'assets/videos/katch-erlangen.mp4',
          img: 'assets/img/katch-erlangen.jpg',
          title: 'Erlangen (Allemagne) - 21 juin 2024',
          text: 'Fête de la Musique',
          type: 'video'
        },
        {
          src: 'assets/img/lm-labos-24.jpeg',
          img: '',
          link: 'https://lundi.am/Ceci-est-une-invitation-6205',
          title: 'Aubervilliers - 15 juin 2024',
          text: "Bloc Party 2",
          type: 'image'
        },
        {
          src: 'assets/img/dlqc_24_1.jpg',
          img: '',
          link: 'https://www.instagram.com/p/C5lb9uiMuwK/',
          title: 'Tulle - 4 avril 2024',
          text: 'Les Lendemains qui Chantent (SMAC)',
          type: 'image'
        },
        {
          src: '',
          img: '',
          title: 'Saint-Jean-Ligoure - 8 décembre 2023',
          text: 'Bistrot Saint Jean',
          type: 'text'
        },
        {
          src: 'assets/img/metallos.jpg',
          img: '',
          link: 'https://www.maisondesmetallos.paris/face-a-face-b-mathieu-bauer',
          title: 'Paris - 25 novembre 2023',
          text: 'La Maison des Metallos',
          type: 'image'
        },
        {
          src: 'https://lpr-camp.org/wp-content/uploads/2023/03/3OK.cleaned-1-1536x1090.jpg',
          img: '',
          link: 'https://lpr-camp.org/blog/2023/08/08/descriptifs-programmation/',
          title: 'Bure - 2 septembre 2023',
          text: 'Terres et communs',
          type: 'image'
        },
        {
          src: 'assets/img/clem-tarnac-2023-edit.jpg',
          img: '',
          title: 'Tarnac - 11 août 2023',
          link: 'https://www.youtube.com/watch?v=ZyDdo_BqPCo',
          text: 'Festival de la maison aux volets rouges',
          type: 'image'
        },
        {
          src: 'assets/videos/NOIR NOIR H264.mov.mp4',
          img: 'assets/img/katchakine-aquarium-2023-1.jpeg',
          link: 'https://www.theatredelaquarium.net/festival-bruit/bruit-du-15-juin-au-1er-juillet/katchakine',
          title: 'Paris - 1er juillet 2023',
          text: "Festival Bruit - Théâtre de l'Aquarium",
          type: 'video'
        },
        {
          src: 'assets/img/L1000182_v5-1024.jpg',
          img: '',
          title: 'Royère de Vassivière - 17 juin 2023',
          text: "L'atelier",
          type: 'image'
        },
        {
          src: 'assets/videos/katch-apo-generale.mp4',
          img: 'assets/img/generale.jpg',
          title: 'Paris - 11 mars 2023',
          text: "La Générale Nord-Est",
          link: 'https://www.lagenerale.fr/fr/projet/journee-des-droits-des-femmes',
          type: 'video'
        },
        {
          src: 'assets/videos/katchakine-limoges-2023-rituel.mp4',
          img: 'assets/img/Screenshot_20230329_000315-720.jpg',
          title: 'Limoges - 28 janvier 2023',
          text: "Les Inouïs - CCM John Lennon",
          type: 'video'
        },
        {
          img: '',
          title: 'Ambrugeat - 1 octobre 2022',
          text: "La Maison sur la Place",
          type: 'image'
        },
        {
          src: 'assets/videos/katchakine-bruxelles-2022.mp4',
          img: 'assets/img/Screenshot_20221018_092852.jpg',
          title: 'Bruxelles - 23 septembre 2022',
          text: "L'Accroche - Soirée Chaudière",
          type: 'video'
        },
        {
          src: 'assets/img/PXL_20220819_171241842.MP.jpg',
          title: 'Nantes - mai 2022',
          text: 'Transfert',
          type: 'image',
        },
        {
          src: "assets/videos/katch-ls-lbm.mp4",
          img: 'assets/img/lundisoir-lbm.jpg',
          link: 'https://www.youtube.com/watch?v=jDY1QJCJzQc',
          title: 'Paris - juin 2022',
          text: 'Lundimatin (Lundisoir) - lien YouTube',
          type: 'video',
        },
        {
          img: '',
          title: 'Cahors - avril 2022',
          text: 'La poule aux potes',
          type: 'text'
        },
        {
          src: '',
          img: '',
          title: 'Tulle - avril 2022',
          text: 'Les Lendemains qui Chantent (SMAC)',
          type: 'text'
        },
        {
          src: 'assets/img/IMG_20211112_182028-774.jpg',
          img: '',
          title: 'Bâle - novembre 2021',
          text: 'Bandspace',
          type: 'image'
        },

        {
          src: '',
          img: '',
          title: 'Tarnac - août 2021',
          text: 'Festival de la maison aux volets rouges',
          type: 'text'
        },

        {
          src: 'assets/img/L1000466_v2-crop-848.jpg',
          img: '',
          title: 'Montreuil - avril 2020',
          text: 'La Parole Errante',
          type: 'image'
        },
        {
          src: 'assets/img/katchakine-aquarium-bruit.png',
          img: '',
          title: 'Paris - janvier 2020',
          text: "Festival Bruit - Théâtre de l'Aquarium",
          type: 'image'
        },
        {
          src: '',
          img: '',
          title: 'Montpellier - mai 2019',
          text: "Théâtre des 13 Vents",
          type: 'text'
        },
        {
          src: 'assets/img/katchakine.webp',
          img: '',
          title: 'Tulle - mars 2019',
          text: "Théâtre l'Empreinte",
          type: 'image'
        },
        {
          src: 'assets/img/IMG_20190129_140032-1040.jpg',
          img: '',
          title: "",
          text: "Quelques fêtes sauvages entre 2020 et 2021...",
          type: 'image'
        },
        /*{
          src: '',
          img: '',
          title: 'Eymoutiers - août 2018',
          text: "Festival Théâtre Râte",
          type: 'image'
        },*/
      ],
      lyrics: [
        { title: 'Où est ton rituel',
          text: "<p>Le serpent de l’aurore a perdu son venin et nous, pauvres damnés, n’avons que notre pain.<br/> \
                De l’amour du fétiche, ne reste que son prix et sa triste amulette d’argent.<br/> \
                Les choses animent les gens qui animent les choses.<br/> \
                Mais qu’en est-il du reste ?<br/> \
                Le mystère est total et pourtant… une question subsiste.<br/> \
                </p> \
                <p>Où est ton rituel ?<br/> \
                Où est ton rituel pauvre homme ? \
                </p> \
                <p>Le mort saisi le vif au cœur.<br/> \
                Le pire est toujours sûr de vivre en dévorant.<br/> \
                Les porcs ont peur de la mort. Et alors?<br/> \
                Qu’en est-il de nous hein ?<br/> \
                Et du reste ? \
                </p> \
                <p>Où est ton rituel ?<br/> \
                Où est ton rituel pauvre homme ? \
                </p> \
                <p>L’avenir flambe en traversant les cieux mais qu’en est-il de toi petit homme ?<br/> \
                Qu’en est-il du feu qui chante le refus ?<br/> \
                Où sont les offrandes pour les dieux inconnus ?<br/> \
                Qu’en est-il de vous plaisirs dionysiaques ?<br/> \
                Libérez-nous de nous et offrez-nous l’orgiaque !<br/> \
                Et la danse endiablée de mon cul dans tout ça ?<br/> \
                On dit qu’on aime ça mais qu’en est-il du reste ? \
                </p> \
                <p>Où est ton rituel ?<br/> \
                Où est ton rituel pauvre homme ? \
                </p> \
                <p>Les choses animent les gens qui animent les choses qui animent les gens <br/> qui animent les choses qui animent les gens qui animent les choses etc… \
                </p> \
                <p>Création créatures ! Je ne vous dis que ça !<br/> \
                La cruelle aventure se doit d’être spirituelle.<br/> \
                Soyons maudits comme votre part monsieur Bataille !<br/> \
                Aussi perdus que votre nu mon cher René ! \
                </p> \
                <p>Où est ton rituel ?<br/> \
                Où est ton rituel pauvre homme ? \
                </p> \
          "
        },
        {
          title: 'Indigène',
          text: "C’est un égotrip\n \
                dans les règles de l’art<br/> \
                Un trip égocentrique<br/> \
                Genre joueur de guitare \
                </p> \
                <p>Katchakine s’élance. S’élance dans la danse.<br/> \
                Et ses fesses qui balances laissent d’utopiques promesses. \
                </p> \
                <p>Katchakine électrique a des problèmes de fric<br/> \
                Mais là elle s’en balance et laisse danser son allégresse \
                </p> \
                <p>Tout ce qu’elle veut c’est de l’amour<br/> \
                Elle en veut tout autour d’elle<br/> \
                Pour étouffer le compte à rebours<br/> \
                Avant qu’on la rappelle. \
                </p> \
                <p>Katchakine indigène qui chasse l’indigeste réel qui lasse<br/> \
                Ce rival et ce temps si cruel, cet ennui sans appel \
                </p> \
                <p>Katchakine a son cri qu’elle envoie dans le bleu des cieux<br/> \
                Elle tourne dans la nuit dévorée par le feu \
                </p> \
                <p>Tout ce qu’elle veut c’est de l’amour<br/> \
                Elle en veut tout autour d’elle<br/> \
                Pour étouffer le compte à rebours<br/> \
                Avant qu’on la rappelle. \
                </p> \
                <p>C’est un égotrip<br/> \
                dans les règles de l’art<br/> \
                Un trip égocentrique<br/> \
                Genre joueur de guitare \
                </p> \
                <p>Katchakine ressort tout ce qui la dévore à mort<br/> \
                C’est pour ça qu’elle se glisse au cœur de ses caprices. \
                </p> \
                <p>Tout ce qu’elle veut c’est de l’amour<br/> \
                Elle en veut tout autour d’elle<br/> \
                Pour étouffer le compte à rebours<br/> \
                Avant qu’on la rappelle. \
                </p> \
          ",
        },
      ],
      tracklist_live: [
        {
          title: 'Où est ton Rituel ?',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/Katchakine%20-%20Ou%20Est%20Ton%20Rituel%20MASTER%205.wav.mp3',
        },
        {
          title: 'Apocalypse',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/Katchakine%20-%20Apocalypse%20MASTER%203.wav.mp3',
        },
        {
          title: 'La Belle Meunière',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/Katchakine%20-%20La%20Belle%20Meuniere%20MASTER%202.wav.mp3',
        },
        {
          title: "J'entends la Mort",
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/J%27entends%20la%20mort%202024-10-07%202226.mp3',
        },
        {
          title: 'Le Diable au Corps',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/Le%20Diable%20au%20Corps%202024-08-27%200315.mp3',
        },
        {
          title: 'Kit Cat Club',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/kit%20kat%20club%20-%20mix%202024-08-24%201825.mp3',
        },
        {
          title: 'La Goutte',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/La%20Goutte%20-%20mix%202024-08-29%200130.mp3',
        },
        {
          title: 'Noire Brûlure (edit)',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/Noire%20Brulure%20-%20mix%203%202024-10-07%202318.mp3',
        },
        {
          title: 'Ma Musique Ma Muse',
          src: 'https://sly.parisson.com/projects/Katchakine/Tracklist/Muse%20-mix%202024-10-07%202326.mp3',
        }
      ],
      text: 'bla bla',
    }
  }
})

function videoEnded(video) {
  video.load();
};


